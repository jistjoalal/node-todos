
$(document).ready(function() {
  $.getJSON('/api/todolists')
  .then(renderTodolists);

  $('#todolistInput').keypress(function(event) {
    if (event.which == 13) {
      createTodolist();
    }
  });

  $('.todolists').on('click', 'span', function(e) {
    e.stopPropagation();
    if (window.confirm('Really Delete?')) {
      removeTodolist($(this).parent().parent());
    }
  })

  $('.todolists').on('click', 'li', function() {
    toggleTodolist($(this));
  });
});

function renderTodolists(todolists) {
  todolists.forEach(function(todolist) {
    renderTodolist(todolist);
  })
}

function renderTodolist(todolist) {
  // create new li element
  var newTodolist = $('<li class="todolist">'
                      + '<p>' + todolist.name
                      + '<span class="delete">delete</span></li></p>');
  newTodolist.data('id', todolist._id);
  newTodolist.data('open', false)
  // add to ul list
  $('.todolists').append(newTodolist);
}

function createTodolist() {
  var usrInput = $('#todolistInput').val();
  $.post('/api/todolists', {name: usrInput})
  .then(function(newTodolist) {
    $('#todolistInput').val('');
    renderTodolist(newTodolist);
  })
  .catch(function(err) {
    console.log(err);
  })
}

function removeTodolist(todolist) {
  var clickedId = todolist.data('id')
  var deleteUrl = '/api/todolists/' + clickedId
  
  $.ajax({
    method: 'DELETE',
    url: deleteUrl
  })
  .then((data) => { todolist.remove() })
  .catch((err) => { console.log(err) })

  $.getJSON('/api/todos/list/' + todolist.data('id'))
  .then((todos) => {
    todos.forEach((todo) => {
      removeTodo(todo)
    })
  })
}

function toggleTodolist(todolist) {
  if (!todolist.data('open')) {
    var input = $('<li><input type="text" id="todoInput" '
                   + 'placeholder="New Todo..."/></li>')
    // attach event listeners
    input.keypress(function(event) {
      if (event.which == 13) {
        createTodo(input.children(0), $(this).parent())
      }
    });
    input.click(function(e) {
      e.stopPropagation()
    });
    todolist.append(input)
    $.getJSON('/api/todos/list/' + todolist.data('id'))
    .then((todos) => {
      todos.forEach((todo) => {
        renderTodoToList(todo, todolist)
      })
      todolist.data('open', true)
    })
  } else {
    todolist.children().each((idx, todo) => {
      if (todo.nodeName === 'LI') {
        todo.remove()
      }
    })
    todolist.data('open', false)
  }
}

function renderTodoToList(todo, todolist) {
  var newTodo = $('<li class="todo">'
                   + todo.text + '<span class="delete">X</span></li>');

  // add event listeners
  newTodo.on('click', 'span', function(e) {
    e.stopPropagation();
    removeTodo($(this).parent())
  })
  newTodo.on('click', function(e) {
    e.stopPropagation();
    updateTodo($(this));
  })

  newTodo.data('id', todo._id);
  newTodo.data('completed', todo.completed);
  // completed?
  if (todo.completed) {
    newTodo.addClass("done");
  }
  todolist.append(newTodo)
}

function createTodo(input, todolist) {
  $.post('/api/todos', { 
    text: input.val(),
    todolist_id: todolist.data('id')
  })
  .then(function(newTodo) {
    input.val('');
    renderTodoToList(newTodo, todolist);
  })
  .catch(function(err) {
    console.log(err);
  })
}

function removeTodo(todo) {
  var clickedId = todo._id || todo.data('id')
  var deleteUrl = '/api/todos/' + clickedId
  $.ajax({
    method: 'DELETE',
    url: deleteUrl
  })
  .then(() => { todo.remove() })
  .catch((err) => { console.log(err) })
}

function updateTodo(todo) {
  var isDone = !todo.data('completed')
  $.ajax({
    method: 'PUT',
    url: '/api/todos/' + todo.data('id'),
    data: {
      completed: isDone,
      updated_date: Date.now
    }
  })
  .then((updatedTodo) => {
    todo.toggleClass('done')
    todo.data('completed', isDone)
  })
}
