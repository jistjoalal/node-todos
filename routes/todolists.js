const express = require('express')
const router = express.Router()
const db = require('../models')

// index
router.get('/', (req, res) => {
  db.Todolist.find()
  .then(function(lists) { res.json(lists) })
  .catch(function(err) { res.send(err) })
})

// create
router.post('/', (req, res) => {
  db.Todolist.create(req.body)
  .then((newTodolist) => { res.status(201).json(newTodolist) })
  .catch((err) => { res.send(err) })
})

// read
router.get('/:todolistId', (req, res) => {
  db.Todolist.findById(req.params.todolistId)
  .then((todolist) => res.json(todolist))
  .catch((err) => res.send(err))
})

// update
router.put('/:todolistId', (req, res) => {
  db.Todolist.findOneAndUpdate(
    { _id: req.params.todolistId }, req.body, { new: true }
  ).then((todolist) => { res.json(todolist) })
  .catch((err) => { res.send(err) })
})

// destroy
router.delete('/:todolistId', (req, res) => {
  db.Todolist.remove({ _id: req.params.todolistId})
  .then(() => { res.json({ message: 'Todolist deleted!' })})
  .catch((err) => { res.send(err) })
})

module.exports = router