const express = require('express')
const router = express.Router()
const db = require('../models')

// index
router.get('/', (req, res) => {
  db.Todo.find()
  .then((todos) => { res.json(todos) })
  .catch((err) => { res.send(err) })
})

// create
router.post('/', (req, res) => {
  db.Todo.create(req.body)
  .then((newTodo) => { res.status(201).json(newTodo) })
  .catch((err) => { res.send(err) })
})

// read
router.get('/:todoId', (req, res) => {
  db.Todo.findById(req.params.todoId)
  .then((todo) => { res.json(todo) })
  .catch((err) => { res.send(err) })
})

// update
router.put('/:todoId', (req, res) => {
  db.Todo.findOneAndUpdate({ _id: req.params.todoId }, req.body, { new: true })
  .then((todo) => { res.json(todo) })
  .catch((err) => { res.send(err) })
})

// destroy
router.delete('/:todoId', (req, res) => {
  db.Todo.remove({ _id: req.params.todoId })
  .then(() => { res.json({ message: 'Todo item deleted!' })})
  .catch((err) => { res.send(err) })
})

// find by todolist
router.get('/list/:todolist_id', (req, res) => {
  db.Todo.find({ todolist_id: req.params.todolist_id })
  .sort({ updated_date: -1 })
  .then((todos) => { res.json(todos) })
})

module.exports = router