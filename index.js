const express = require('express')
const app = express()
const port = process.env.PORT || 3000

// body parser
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

// assets
app.use(express.static(__dirname + '/views'))
app.use(express.static(__dirname + '/public'))

// routes
// todos
const todoRoutes = require('./routes/todos')
app.use('/api/todos', todoRoutes)
// todolists
const todolistRoutes = require('./routes/todolists')
app.use('/api/todolists', todolistRoutes)
// root
app.get('/', (req, res) => {
  res.sendFile('index.html')
})

// run server
app.listen(port, () => {
  console.log(`App started on port ${port}...`)
})