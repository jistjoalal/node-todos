const mongoose = require('mongoose')

const todolistSchema = new mongoose.Schema({
  name: {
    type: String,
    required: 'Todolist name cant be blank!'
  }
})

const Todolist = mongoose.model('Todolist', todolistSchema)
module.exports = Todolist
