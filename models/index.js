const mongoose = require('mongoose')
mongoose.set('debug', true)
mongoose.set('useFindAndModify', false)
mongoose.connect('mongodb://localhost/todo-api', {
  useCreateIndex: true,
  useNewUrlParser: true
})
mongoose.Promise = Promise

module.exports.Todo = require('./todo')
module.exports.Todolist = require('./todolist')