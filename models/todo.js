const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId

const todoSchema = new mongoose.Schema({
  todolist_id: {
    type: ObjectId,
    required: 'Todo todolist_id cant be blank!'
  },
  text: {
    type: String,
    required: 'Todo text cant be blank!'
  },
  completed: {
    type: Boolean,
    default: false
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  updated_date: {
    type: Date,
    default: Date.now
  }
})

const Todo = mongoose.model('Todo', todoSchema)
module.exports = Todo
